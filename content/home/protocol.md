+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "The Protocol"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "white"
  
  # Background gradient.
  # gradient_start = "#faf9d8"
  # gradient_end = "#d1e4ba"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

The Intervention Mapping (IM) protocol (Bartholomew et al., 2016) describes the iterative path from problem identification to problem solving or mitigation. Each of the six steps of IM comprises several tasks each of which integrates theory and evidence. The completion of the tasks in a step creates a product that is the guide for the subsequent step. The completion of all of the steps serves as a blueprint for designing, implementing and evaluating an intervention based on a foundation of theoretical, empirical and practical information.

The six steps and related tasks of the IM process are:

1. Conduct a needs assessment or problem analysis, identifying what, if anything, needs to be changed and for whom;
2. Create matrices of change objectives by combining (sub-)behaviors (performance objectives) with behavioral determinants, identifying which beliefs should be targeted by the intervention;
3. Select theory-based intervention methods that match the determinants into which the identified beliefs aggregate, and translate these into practical applications that satisfy the parameters for effectiveness of the selected methods;
4. Integrate methods and the practical applications into an organized program;
5. Plan for adoption, implementation and sustainability of the program in real-life contexts;
6. Generate an evaluation plan to conduct effect and process evaluations.

<!--- IMG -->

Intervention Mapping is a planning approach that is based on using theory and evidence as foundations for taking an ecological approach to assessing and intervening in health problems and engendering community participation. The key words in IM are planning, research and theory. IM provides a vocabulary for program planning, procedures for planning activities, and technical assistance with identifying theory-based determinants and matching them with appropriate methods for change.

Intervention Mapping is not a new theory or model; it is an additional tool for the planning and development of health promotion interventions. It maps the path from recognition of a need or problem to the identification of a solution. Although Intervention Mapping is presented as a series of steps, Bartholomew and colleagues (2016) see the planning process as iterative rather than linear. Program planners move back and forth between tasks and steps. The process is also cumulative: Each step is based on previous steps, and inattention to a particular step may lead to mistakes and inadequate decisions.

<p><strong>Step 1: Logic Model of the Problem</strong></p>
<p>In Step 1, before beginning to actually plan an intervention, the planner assesses the health problem, its related behavior and environmental conditions, and their associated determinants for the at-risk populations. This assessment encompasses two components: a scientific, epidemiologic, behavioral, and social perspective of an at-risk group or community and its problems; and an effort to &#8220;get to know,&#8221; or begin to understand, the character of the community, its members, and its strengths. The product of this first step is a description of a health problem, its impact on quality of life, behavioral and environmental causes and determinants of behavior and environmental causes.</p>
<p>In Step 1, the planner completes the following tasks:</p>
<ol>
<li>Establish and work with a planning group</li>
<li>Conduct a needs assessment to create a logic model of the problem</li>
<li>Describe the context for the intervention, including the population, setting, and community</li>
<li>State program goals</li>
</ol>
<p><strong>Step 2: Program Outcomes and Objectives – Logic Model of Change</strong></p>
<p>Step 2 provides the foundation for the intervention by specifying who and what will change as a result of the intervention. The product of Step 2 is a set of matrices of selected ecological levels (i.e., individual through societal) that combines performance objectives for each level with selected personal and external determinants to produce change objectives, the most immediate target of an intervention. In order to develop performance objectives beyond the individual, roles are identified at each selected ecological level. Statements of what must be changed at each ecological level and who must make the change, are more specific intervention foci than are traditional program goals and objectives.</p>
<p>In Step 2 the planner completes the following tasks:</p>
<ol>
<li>State expected outcomes for behavior and environment</li>
<li>Specify performance objectives for behavioral and environmental outcomes</li>
<li>Select determinants for behavioral and environmental outcomes</li>
<li>Construct matrices of change objectives</li>
<li>Create a logic model of change</li>
</ol>
<p><strong>Step 3 &#8211; Program Design</strong></p>
<p>In Step 3, the planner seeks theory-informed methods and practical strategies to effect changes in the health behavior of individuals and related small groups and to change organizational and societal factors to affect the environment. An intervention method is a defined process by which theory postulates and empirical research provides evidence for how change may occur in the behavior of individuals, groups, or social structures. Whereas a method is a theory-based technique to influence behavior or environmental conditions, a strategy is a way of organizing and operationalizing the intervention methods.</p>
<p>In Step 3, the planner completes the tasks of:</p>
<ol>
<li>Generate program themes, components, scope, and sequence</li>
<li>Choose theory- and evidence-based change methods</li>
<li>Select or design practical applications to deliver change methods</li>
</ol>
<p><strong>Step 4: Program production</strong></p>
<p>The products in Step 4 include a description of the scope and sequence of the components of the intervention, completed program materials, and program protocols. This step demands the careful reconsideration of the intended program participants and the program context. It also requires pilot testing of program strategies and materials with intended implementers and recipients. This step gives specific guidance for communicating program intent to producers (e.g., graphic designers, videographers, and writers).</p>
<p>In Step 4, the planner completes the following tasks:</p>
<ol>
<li>Refine program structure and organization</li>
<li>Prepare plans for program materials</li>
<li>Draft messages, materials, and protocols</li>
<li>Pretest, refine, and produce materials</li>
</ol>
<p><strong>Step 5: Program Implementation Plan</strong></p>
<p>The focus of Step 5 is program adoption and implementation (including consideration of program sustainability). Of course, considerations for program implementation actually begin as early as the needs assessment and are revisited in this step. The step requires the process of matrix development exactly like that in Step 2 except that these matrices are developed with adoption and implementation performance objectives juxtaposed to personal and external determinants. The linking of each performance objective with a determinant produces a change objective to promote program adoption and use. These objectives are then operationalized using methods and strategies to form theory-informed plans for adoption and implementation. The product for Step 5 is a detailed plan for accomplishing program adoption and implementation by influencing behavior of individuals or groups who will make decisions about adopting and using the program.</p>
<p>In Step 5, the planner completes the following tasks:</p>
<ol>
<li>Identify potential program users (adopters, implementers, and maintainers)</li>
<li>State outcomes and performance objectives for program use</li>
<li>Construct matrices of change objectives for program use</li>
<li>Design implementation interventions</li>
</ol>
<p><strong>Step 6: Evaluation</strong></p>
<p>In Step 6, the planner finalizes an evaluation plan that is actually begun in the needs assessment and is developed along with the intervention map. In the process of Intervention Mapping, planners make decisions about change objectives, methods, strategies, and implementation. The decisions, although informed by theory and evidence from research, still may not be optimal or may even be completely wrong. Through effect and process evaluation, planners can determine whether decisions were correct at each mapping step. To evaluate the effect of an intervention, researchers analyze the change in health and quality of life problems, behavior and environment, and determinants of performance objectives. All these variables have been defined in a measurable way during the preceding steps. The product of Step 6 is a plan for answering these questions.</p>
<p>In Step 6, the planner completes the following tasks:</p>
<ol>
<li>Write effect and process evaluation questions</li>
<li>Develop indicators and measures for assessment</li>
<li>Specify the evaluation design</li>
<li>Complete the evaluation plan</li>
</ol>
