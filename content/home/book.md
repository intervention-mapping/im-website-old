+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "The Book"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "white"
  
  # Background gradient.
  # gradient_start = "#faf9d8"
  # gradient_end = "#d1e4ba"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

<!--- IMG -->

![Book Cover](img/fourth-edition.jpg)

The new 4<sup>th</sup> edition of the Intervention Mapping book has appeared in February 2016.

Thoroughly revised and updated, Planning Health Promotion Programs provides a powerful, practical resource for the planning and development of health education and health promotion programs.

In addition, the book has been redesigned to be more teachable, practical, and practitioner-friendly.

The book can be ordered [here](http://www.wiley.com/WileyCDA/WileyTitle/productCd-111903549X.html).

To start learning about Intervention Mapping immediately, a number of accessible papers introducing Intervention Mapping are freely available at the [Effective Behavior Change website](https://effectivebehaviorchange.com/).

