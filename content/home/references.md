+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 60  # Order that this section will appear.

title = "Publications"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "white"
  
  # Background gradient.
  # gradient_start = "#faf9d8"
  # gradient_end = "#d1e4ba"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

## History</strong>

<p>Bartholomew Eldrigde, L. K., Markham, C. M., Ruiter, R. A. C., Fernàndez, M. E., Kok, G., &amp; Parcel, G. S. (2016). Planning health promotion programs: An Intervention Mapping approach (4th ed.). Hoboken, NJ: Wiley.</p>

<p>Bartholomew, L. K., Parcel, G. S., Kok, G., Gottlieb, N. H., &amp; Fernández, M. E. (2011). Planning health promotion programs: An Intervention Mapping approach (3rd ed.). San Francisco, CA: Jossey-Bass.</p>

<p>Bartholomew, L. K., Parcel, G. S., Kok, G., &amp; Gottlieb, N. H. (2006). Intervention Mapping: Designing theory and evidencebased health promotion programs. San Francisco, CA: Jossey-Bass.</p>

<p>Bartholomew, L. K., Parcel, G. S., Kok, G., &amp; Gottlieb, N. H. (2001). Intervention Mapping: Designing theory and evidencebased health promotion programs. Mountain View, CA: Mayfield Publishing.</p>

<p>Bartholomew, L. K., Parcel, G. S., &amp; Kok, G. (1998). Intervention Mapping: A process for developing theory- and evidence based health education programs. Health Education and Behavior, 25(5), 545–563.</p>

## Selected relevant recent publications

<p><a href="http://ehps.net/ehp/index.php/contents/article/download/ehp.v16.i5.p142/7" target="_blank">Peters, G.-J. Y. (2014). A practical guide to effective behavior change: How to identify what to change in the first place. European Health Psychologist, 16(4), 142–155.</a></p>

<p><a href="http://www.ehps.net/ehp/index.php/contents/article/download/ehp.v16.i5.p156/8" target="_blank">Kok, G. (2014). A practical guide to effective behavior change: How to apply theory- and evidence-based behavior change methods in an intervention. European Health Psychologist, 16(5), 156–170.</a></p>

<p><a href="http://www.amazon.com/Health-Behavior-Research-Practice-Jossey-Bass/dp/1118628985?pf_rd_mnb=ATVPDKIKX0D34&amp;pf_rd_stb=center-2&amp;pf_rd_rat=0817NMRY4ZRQZM6P18TH&amp;pf_rd_t3r=101&amp;pf_rd_ptd=470938631&amp;pf_rd_ied=507846&amp;tag=buaazs-20&amp;pf_rd_ptd=470938631&amp;pf_rd_ied=507846" target="_blank">Bartholomew, L. K., Markham, C., Mullen, P., &amp; Fernandez, M. E. (2015). Planning models for theory-based health promotion interventions. In: K. Glanz, B. K. Rimer, &amp; K. Viswanath (Eds.),Health Behavior: Theory, Research, and Practice, pp. 359-xxx. San Francisco, CA: John Wiley &amp; Sons. ISBN: <span class="a-size-base a-color-base"> 978-1118628980</span></a></p>

<p><a href="http://dx.doi.org/10.1080/17437199.2015.1077155" target="_blank">Kok, G., Gottlieb, N. H., Peters, G.-J. Y., Mullen, P. D., Parcel, G. S., Ruiter, R. A. C., Fernández, M. E., Markham, C., &amp; Bartholomew, L. K. (2015). A Taxonomy of behavior change methods; an Intervention Mapping approach. Health Psychology Review, online. DOI:10.1080/17437199.2015.1077155</a></p>

<p><a href="http://dx.doi.org/10.4278/ajhp.22.6.437" target="_blank">Kok, G., Gottlieb, N., Commers, M., &amp; Smerecnik, C. (2008). The ecological approach in health promotion programs: A decade later. American Journal of Health Promotion, 22, 437–442. doi:10.4278/ajhp.22.6.437</a></p>

<p><a href="http://dx.doi.org/10.1186/1471-2458-12-1037" target="_blank">Kok, G., Gottlieb, N. H., Panne, R., &amp; Smerecnik, C. (2012). Methods for environmental change: An exploratory study. BMC Public Health, 12(1), 1037. doi:10.1186/1471-2458-12-1037</a></p>

<p><a href="http://dx.doi.org/10.1080/17437199.2013.848409" target="_blank">Peters, G.-J. Y., de Bruin, M., &amp; Crutzen, R. (2015). Everything should be as simple as possible, but no simpler: Towards a protocol for accumulating evidence regarding the active content of health behaviour change interventions. Health Psychology Review, 9(1), 1–14. doi:10.1080/17437199.2013.848409</a></p>

<p><a href="http://dx.doi.org/10.1080/17437199.2014.981833" target="_blank">De Bruin, M., Crutzen, R., &amp; Peters, G.-J. Y. (2015). Everything should be as simple as possible, but this will still be complex: A reply to various commentaries on IPEBA. Health Psychology Review, 9(1), 38–41. doi:10.1080/17437199.2014.981833</a></p>

<p><a href="http://dx.doi.org/10.1111/jar.12017" target="_blank">Schaafsma, D., Stoffelen, J. M. T., Kok, G., &amp; Curfs, L. M. G. (2013). Exploring the development of existing sex education programmes for people with intellectual disabilities: An intervention mapping approach. Journal of Applied Research in Intellectual Disabilities: JARID, 26(2), 157–166. doi:10.1111/jar.12017</a></p>
