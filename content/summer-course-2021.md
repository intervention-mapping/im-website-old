+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = false  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

title = "Summer Course 2021"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "white"
  
  # Background gradient.
  # gradient_start = "#faf9d8"
  # gradient_end = "#d1e4ba"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

# Intervention Mapping Summer Course 2021

Intervention Mapping: Designing Theory-based and Evidence-based Programs
July 5-9 | 2021

{{% alert note %}}

## INTERVENTION MAPPING SUMMER COURSE REGISTRATION HAS NOW CLOSED

Please note that the registration for the Intervention Mapping Summer Course is now closed.
All registered participants will be informed soon about the details of the course.
In case you have any questions, please contact registration@interventionmapping.com.

{{% /alert %}}

## UPDATE

Due to the COVID-19 pandemic and after lengthy and thoughtful deliberation, the organization has decided to change the Intervention Mapping summer course to a virtual course.
For this reason, some adjustments have been made.

The summer course will take place during the same dates. The fee is € 1.250,00 and includes the virtual environment in which the course will happen.

The course book is not included in the course anymore due to the diversity of attending countries and the shipping expenses involved, our apologies for this inconvenience. To participate successfully in the course, it is mandatory to possess the following book:

Bartholomew Eldredge, L.K., Markham, C. M., Ruiter, R.A.C., Fernández, M.E., Kok, G. & Parcel, G.S., (2016). Planning health promotion programs: An Intervention Mapping approach (4th ed.). San Francisco, CA: Jossey-Bass. ISBN (hardcopy): 978-1-119-03549-7; ISBN (epdf): 978-1-119-03556-5; ISBN (epub): 978-1-119-03539-8

## INITIATOR

The organizer of this course is the section Applied Social Psychology of the Faculty of Psychology and Neuroscience at Maastricht University in the Netherlands.

## INTRODUCTION

The summer course will focus on developing theory-based and evidence-based interventions applied to health promotion and disease prevention. The emphasis of this course will be on applying Intervention Mapping to a pre-selected case but with transfer to participants’ own projects. Intervention Mapping is a protocol for the design of health promotion and behavior change programs, guiding health promoters through a series of steps that will assist them in theory-based and evidence based program development.

## OBJECTIVES

Course participants will apply Intervention Mapping to a pre-selected health problem in the domain of sexual and reproductive health. Participants will be able to design their own effective theory-based and evidence-based prevention intervention for that issue as well as ask for consultations on their own projects.

## CONTENTS

Introduction to, and application of Intervention Mapping; orientation to health promotion and behavior change; approaches to using theory; health promotion program case examples; review of theories used in health promotion.

## COURSE FORMAT

Plenary sessions will cover the principles and processes of Intervention Mapping and include examples of health promotion programs that have successfully applied the Intervention Mapping process. Small groups will discuss and apply the process to the selected health promotion topics. Participants can receive individual counselling on their own topic of interest.

## CERTIFICATE

Upon completion of the course participants will receive a certificate conditional upon their active participation.

## APPLICANT PROFILE

Enrolment in the summer course Intervention Mapping is open to participants with a degree or comparable training in public health with an interest in health promotion. Individuals who are responsible for the development and evaluation of health promotion programs are especially encouraged to apply.

## CAPACITY

The maximum number of participants is 50 (acceptance is on a first-come, first-served basis).

## LANGUAGE

The language of instruction in the course is English.

## LOCATION

The course will be held online. The links to participate will be provided to the students who have completed their registration.

## FEE AND PAYMENT

The course fee is € 1.250,- and includes the virtual environment in which the course will happen.

The course book is not included in the course anymore due to the diversity of attending countries and the shipping expenses involved, our apologies for this inconvenience. To participate successfully in the course, it is mandatory to possess the following book:
Bartholomew Eldredge, L.K., Markham, C. M., Ruiter, R.A.C., Fernández, M.E., Kok, G. & Parcel, G.S., (2016). Planning health promotion programs: An Intervention Mapping approach (4th ed.). San Francisco, CA: Jossey-Bass. ISBN (hardcopy): 978-1-119-03549-7; ISBN (epdf): 978-1-119-03556-5; ISBN (epub): 978-1-119-03539-8

Payment of the course fee must be made in advance. Registration only becomes definitive once payment is received. All payments, indicating the participant’s family name and the course must be remitted in Euros and made payable to the Klinkhamer Group.

## INTERVENTION MAPPING STEPS

1. Needs Assessment
2. Program Outcomes and Objectives
3. Program Plan
4. Program Production
5. Implementation Plan
6. Evaluation Plan

The 4th edition of the book: Bartholomew Eldredge, L.K., Markham, C.M., Ruiter, R.A.C., Fernández, M.E., Kok, G. & Parcel, G.S., 2016; Planning Health Promotion Programs. An Intervention Mapping Approach, 4th edition, San Francisco CA: Wiley, will be used in the course. Participants are advised to read in advance: Kok, G., Peter, L.W.H., & Ruiter, R.A.C. (2017). Planning theory- and evidence-based behavior change interventions: a conceptual review of the Intervention Mapping protocol. Psicologia: Reflexão e Crítica, 30(19). doi: 10.1186/s41155-017-0072-x (open access).

## PROGRAM

Please note, the times below are given in CEST.

### Monday 5 July 2021

- 10.00 – 10.30 Welcome and general introduction to course – Rob Ruiter, Rik Crutzen, Gerjo Kok
- 10.30 – 11.00 Introduction to Intervention Mapping – Gerjo Kok
- 11.15 – 12.15 Step 1: Needs Assessment – Rik Crutzen
- 13.00 – 14.30 Small Groups: IM Step 1 Needs Assessment
- 14.45 – 15.30 Feedback on Step 1 – Rik Crutzen, Gerjo Kok, Rob Ruiter

### Tuesday 6 July 2021

- 10.00 – 11.00 Using Theory in IM – Rob Ruiter
- 11.15 – 12.15 IM Step 2a: Performance Objectives – Gerjo Kok
- 13.00 – 14.30 Small Groups: IM Step 2a Performance Objectives
- 14.45 – 15.30 IM Step 2b: Change Objectives – Rik Crutzen

### Wednesday 7 July 2021

- 10.00 – 12.30 Small Groups: IM Step 2b Change Objectives
- 12.45 – 13.45 IM Step 3: Methods and Applications – Gjalt-Jorn Peters
- 14.00 – 15.30 Small Groups: IM Step 3 Methods and Applications

### Thursday 8 July 2021

- 10.00 – 11.00 Small Groups: Compiling IM Step1-3 into ABCD
- 11.15 – 12.15 IM Step 4: Organizing Applications into Programs – Francine Schneider
- 13.00 – 14.30 Small Groups: IM Step 4 Organizing Applications into Programs
- 14.45 – 15.30 Demonstration project – Christine Markham

### Friday 9 July 2021

- 10.00 – 11.00 Small Groups: Finalize ABCD
- 11.15 – 12.30 Wrap up – All
- 13.15 – 14.15 IM Step 5: Implementation Mapping – Gill ten Hoor
- 14.30 – 15.30 FAQ and Farewell – All

### Control room

Rob Ruiter, Rik Crutzen, Francine Schneider

### Group work tutors

Gill ten Hoor, Karen Schelleman-Offermans, Charlotte Anraad, Tamika Marcos, Francine Schneider, Rik Crutzen, Rob Ruiter

Besides the official program, feel free to ask for opportunities to ask questions about your own project.

## COURSE LEADERS

Robert A.C. Ruiter and Gerjo Kok, Dept. Work & Social Psychology, Maastricht University;

Rik Crutzen, Dept. of Health Promotion, Maastricht University;

María E. Fernández Christine M. Markham,

Guy S. Parcel and Nell H. Gottlieb, School of Public Health, University of Texas at Houston (TX).

## FURTHER INFORMATION

If you require any further information concerning the course, please contact the course secretariat via e-mail: registration@interventionmapping.com
