<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <meta name="robots" content="noindex, nofollow" />
    <title>
      Intervention Mapping lectures
    </title>
    <link rel="stylesheet" type="text/css" href="http://selfreflection.eu/portfolio.css" />
  </head>
  <body>
    <div class="all">
      <div class="content">
        <h1>
          Intervention Mapping lectures
        </h1>
        This is a list of Intervention Mapping lectures.

<?php
    echo("<table border=\"0\">");
    $allfiles = scandir("files/");
    foreach ($allfiles as $filename) {
      if  (($filename !== "..") && ($filename !== "."))
      {
        echo("<tr><td>");
        switch (strtolower(substr($filename, -3))) {
          case "pdf":
            echo("<img border=\"0\" src=\"Adobe-PDF-Document-icon.png\" />");
            break;
          case "swf":
            echo("<img border=\"0\" src=\"File-Adobe-Flash-SWF-01-icon.png\" />");
            break;
          default:
            // Do something here;
            break;
        }
        ?>
        </td><td>
        <a class="navigation" href="<?php echo "files/".$filename; ?>"><?php echo $filename; ?></a>
        </tr>
        <?php
      }
    }
    echo("</table>");
?>
        <br /><br />
        These lectures are in these formats:<br />
        <br />
        <table border="0">
          <tr>
            <td><img border="0" src="Adobe-PDF-Document-icon.png" /></td>
            <td>
              PDF, Portable Document Format: these can be opened with prorgams in this list: <a href="http://en.wikipedia.org/wiki/List_of_PDF_software">http://en.wikipedia.org/wiki/List_of_PDF_software</a>.
            </td>
          </tr>
          <tr>
            <td><img border="0" src="File-Adobe-Flash-SWF-01-icon.png" /></td>
            <td>
              SWF, Adobe Flash format: these can be viewed directly in your browser. You do however need a plugin; you can download it for free at <a href="http://get.adobe.com/flashplayer/">http://get.adobe.com/flashplayer/</a> of <a href="http://get.adobe.com/shockwave/">http://get.adobe.com/shockwave/</a>.
            </td>
          </tr>
        </table>
      </div> 
    </div>
  </body>
</html>